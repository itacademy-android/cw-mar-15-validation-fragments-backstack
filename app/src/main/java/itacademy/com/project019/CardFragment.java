package itacademy.com.project019;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CardFragment extends Fragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.lvCards)
    ListView lvCards;
    @BindView(R.id.etCard)
    EditText etCard;
    @BindView(R.id.etExpiryDate)
    EditText etExpiryDate;
    @BindView(R.id.btnCreate)
    Button btnCreate;

    private ArrayList<String> cardList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etCard.addTextChangedListener(watcher);
        etExpiryDate.addTextChangedListener(dateWatcher);

        lvCards.setOnItemClickListener(this);
    }

    @OnClick(R.id.btnCreate)
    void onCreateCard() {
        validateCard();
    }

    private void validateCard() {
        if (etCard.getText().toString().isEmpty()) {
            etCard.setError(getString(R.string.enter_card_number));
            return;
        }

        if (etExpiryDate.getText().toString().isEmpty()) {
            etExpiryDate.setError(getString(R.string.enter_expiry_date));
            return;
        }

        if (!etCard.getText().toString().equals("4242 4242 4242 4242")) {
            etCard.setError(getString(R.string.card_is_invalid));
            return;
        }

        if (!etExpiryDate.getText().toString().equals("01/10")) {
            etExpiryDate.setError(getString(R.string.date_is_invalid));
            return;
        }

        if (cardList.contains(etCard.getText().toString())) {
            Snackbar.make(getActivity().findViewById(android.R.id.content),
                    "Card already exists", Snackbar.LENGTH_LONG).show();
            return;
        }

        cardList.add(etCard.getText().toString());
        updateCardList(cardList);
    }

    private void updateCardList(ArrayList<String> cardData) {
        lvCards.setAdapter(new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1, cardData));
    }

    private void switchFragment(Fragment fragment, String cardNumber) {
        fragment.setArguments(getCardData(cardNumber));

        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.add(R.id.fragmentContainer, fragment).commit();
    }

    private Bundle getCardData(String cardNumber) {
        Bundle bundle = new Bundle();
        bundle.putString("cardNumber", cardNumber);
        return bundle;
    }

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 4) {
                if (s.charAt(4) != ' ') {
                    s.insert(4, " ");
                }
            }

            if (s.length() > 9) {
                if (s.charAt(9) != ' ') {
                    s.insert(9, " ");
                }
            }

            if (s.length() > 14) {
                if (s.charAt(14) != ' ') {
                    s.insert(14, " ");
                }
            }

            if (s.toString().length() == 19) {
                etExpiryDate.requestFocus();
            }
        }
    };

    TextWatcher dateWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 2) {
                if (s.charAt(2) != '/') {
                    s.insert(2, "/");
                }
            }
        }
    };

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        String cardNumber = (String) adapterView.getItemAtPosition(position);
        switchFragment(new PaymentFragment(), cardNumber);
    }
}
